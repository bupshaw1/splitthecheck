Rails.application.routes.draw do
  devise_for :users
  resources :restaurants, except: [:edit, :delete, :update] do
    resources :comments
    collection do
      get :summary
    end
    member do
     post 'will_split', to: 'restaurants#will_split'
     post 'wont_split', to: 'restaurants#wont_split'
     post 'tag', to: 'restaurants#tag'
    end
  end

  root to: 'restaurants#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

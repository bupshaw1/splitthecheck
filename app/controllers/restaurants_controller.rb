class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]

  # GET /restaurants
  # GET /restaurants.json
  def index
    @restaurants = Restaurant.all.order('name ASC')
    if params[:name] || params[:location]
      @restaurants = Restaurant.search(params[:name], params[:location]).order('name ASC')
        if @restaurants.blank?
          redirect_to restaurants_path, flash: {notice: "No match."}
        end
    else
      @restaurants = Restaurant.all.order('name ASC')
    end
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @comments = Comment.where(restaurant_id: @restaurant).order("created_at DESC")
  end

  # GET /restaurants/new
  def new
    @restaurant = current_user.restaurants.build
  end

  # GET /restaurants/1/edit
#  def edit
#  end

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = current_user.restaurants.build(restaurant_params)
    @restaurant.will_split = 0
    @restaurant.wont_split = 0
    @restaurant.tag_list = @restaurant.name

    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1
  # PATCH/PUT /restaurants/1.json
#  def update
#    respond_to do |format|
#      if @restaurant.update(restaurant_params)
#        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
#        format.json { render :show, status: :ok, location: @restaurant }
#      else
#        format.html { render :edit }
#        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
#      end
#    end
#  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
#  def destroy
#    @restaurant.destroy
#    respond_to do |format|
#      format.html { redirect_to restaurants_url, notice: 'Restaurant was successfully destroyed.' }
#      format.json { head :no_content }
#    end
#  end

  def will_split
    @restaurant = Restaurant.find(params[:id])
    @restaurant.vote_by :voter => current_user, :vote => 'like', :duplicate => true
    redirect_to restaurants_path, flash: {notice: "Will Split vote added!"}
  end

  def wont_split
    @restaurant = Restaurant.find(params[:id])
    @restaurant.vote_by :voter => current_user, :vote => 'bad', :duplicate => true
    redirect_to restaurants_path, flash: {notice: "Won't Split vote added!"}
  end 

  def tag
    @restaurant = Restaurant.find(params[:id])
    if current_user.tag_list.include?(@restaurant.name)
      current_user.tag_list.delete(@restaurant.name)
      current_user.save
    else
      current_user.tag_list += @restaurant.name
      current_user.save
    end
    redirect_to restaurants_path
  end

  def summary
    @user = current_user.email
    @comments = Comment.joins(:restaurant).where(:comments => { :name => @user })
    @restaurants = Restaurant.all.order('name ASC')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :address, :state, :will_split, :wont_split, :tag_list)
    end
end

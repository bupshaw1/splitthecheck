class Comment < ApplicationRecord
  belongs_to :restaurant
  belongs_to :user

  validates :name, :content, presence: true
end

class Restaurant < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  acts_as_votable
  acts_as_taggable
  validates :name, :address, :state, presence: true
  validates :name, uniqueness: {
    scope: [:address, :state],
    message: " this restaurant has already been added."
  }
  validates :state, format: {
    with: /\A([A-Z]{2,2})\z/,
    message: "State must be in two letter format."
  }

  def self.search(search_name, search_location)
    if !search_name.blank? && !search_location.blank?
      where("name LIKE ? AND address LIKE ? OR state LIKE ?", "%#{search_name}%", "%#{search_location}%", "%#{search_location}%")
    elsif search_name.blank?
      where("address LIKE ? OR state LIKE ?", "%#{search_location}%", "%#{search_location}%")
    elsif search_location.blank?
      where("name LIKE ?", "%#{search_name}%")
    else
      return scoped
    end
  end  
end 
